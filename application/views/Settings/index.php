<div class="main-content">
  <div class="row">
    <div class="col-sm-6 col-lg-3">
        <div class="overview-item overview-item--c4">
            <div class="overview__inner">
                <div class="overview-box clearfix">
                    <div class="icon">
                        <i class="fa fa-list"></i>
                    </div>
                    <div class="text">
                        <h2>Ajustes de Catálogos</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-lg-3">
        <div class="overview-item overview-item--c4">
            <div class="overview__inner">
                <div class="overview-box clearfix">
                    <div class="icon">
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="text">
                        <h2>Ajustes de Usuario</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6 col-lg-3">
        <div class="overview-item overview-item--c4">
            <div class="overview__inner">
                <div class="overview-box clearfix">
                    <div class="icon">
                        <i class="fa fa-gear"></i>
                    </div>
                    <div class="text">
                        <h2>Ajustes</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
