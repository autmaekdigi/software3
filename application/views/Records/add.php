
<script type="text/javascript">
$( function() {
  $( "#birthdate" ).datepicker({changeYear: true, minDate: new Date(1945, 1 - 1, 1)});
} );
</script>
<div class="main-content">
  <div class="section__content section__content--p30">
    <div class="container-fluid">
    <h1><?= $module_name ?></h1>
    <?= form_open('records/add_firststep') ?>
    <div class="row">
      <div class="col-lg-6">
        <div class="card">
            <div class="card-header">
                <strong>Información personal</strong>
            </div>
            <div class="card-body card-block">
                <div class="form-group">
                    <label for="identification" class=" form-control-label">Identificaión</label>
                    <?php
                    $attrid = array('id' => 'identification', 'placeholder' => 'Identificaión', 'class' => 'form-control', 'required'=>true, 'name'=> 'identification');
                    echo form_input($attrid);
                     ?>
                </div>
                <div class="form-group">
                    <label for="name" class=" form-control-label">Nombre</label>
                    <?php
                    $attrname = array('id' => 'name', 'placeholder' => 'Nombre', 'class' => 'form-control', 'required'=>true, 'name'=> 'name');
                    echo form_input($attrname);
                     ?>
                </div>
                <div class="form-group">
                    <label for="birthdate" class=" form-control-label">Fecha de nacimiento</label>
                    <?php
                    $attrbirth = array('id' => 'birthdate', 'placeholder' => 'Fecha de nacimiento', 'class' => 'form-control', 'required'=>true, 'name'=> 'birthdate');
                    echo form_input($attrbirth);
                     ?>
                </div>
                <div class="row form-group">
                    <div class="col-8">
                        <div class="form-group">
                            <label for="status" class=" form-control-label">Estado Civil</label>
                            <?php

                            foreach ($status as $attr)
                            {
                              $attrstatus[$attr->EstadoCivil_Cod] = $attr->EstadoCivil_Desc;
                            }

                            echo form_dropdown('status',$attrstatus, '1', 'class="form-control select2"');
                             ?>
                        </div>
                    </div>
                    <div class="col-8">
                        <div class="form-group">
                            <label for="nationality" class=" form-control-label">Nacionalidad</label>
                            <?php

                            foreach ($nationality as $attr)
                            {
                              $attrnat[$attr->Nacionalidad_Cod] = $attr->Nacionalidad_Desc;
                            }

                            echo form_dropdown('nationality',$attrnat, '1', 'class="form-control select2"');
                             ?>
                          </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="scholarity" class=" form-control-label">Escolaridad</label>
                    <?php
                    foreach ($scholarity as $attr)
                    {
                      $attrsch[$attr->Escolaridad_Cod] = $attr->Escolaridad_Desc;
                    }

                    echo form_dropdown('scholarity',$attrsch, '1', 'class="form-control select2"');
                     ?>

                   </div>
            </div>
        </div>
      </div>

      <div class="col-lg-6">
        <div class="card">
          <div class="card-header">
              <strong>Localización</strong>
          </div>
          <div class="col-8">
          <div class="form-group">
              <label for="prov" class=" form-control-label">Provincia</label>
              <?php
              $attrprov = array('1' => 'San Jose', '2' => 'Alajuela', '3' => 'Cartago', '4'=>'Heredia', '5' => 'Guanacaste', '6'=>'Puntarenas', '7'=> 'Limón');// this will be displayed by the DB
              echo form_dropdown('prov',$attrprov, '1', 'class="form-control select2"');
               ?>

             </div>

             <div class="form-group">
                 <label for="cant" class=" form-control-label">Cantón</label>
                 <?php
                 $attrcant = array('1' => 'San Jose');// this will be displayed by the DB
                 echo form_dropdown('cant',$attrcant, '1', 'class="form-control select2"');
                  ?>
              </div>
              <div class="form-group">
                  <label for="distr" class=" form-control-label">Distrito</label>
                  <?php
                  $attrcant = array('1' => 'San Jose');// this will be displayed by the DB
                  echo form_dropdown('distr',$attrcant, '1', 'class="form-control select2"');
                   ?>
              </div>
          </div>
        </div>
        <div class="card">
          <div class="card-header">
              <strong>Contacto</strong>
          </div>
          <div class="col-8">
            <div class="form-group">
                <label for="contact_phone" class="form-control-label">Teléfono de Localización</label>
                <?php
                $attrphone = array('id' => 'contact_phone', 'placeholder' => 'Teléfono de Localización', 'class' => 'form-control input_phone', 'required'=>true, 'name'=>'contact_phone');
                echo form_input($attrphone);
                 ?>
            </div>
            <div class="form-group">
                <label for="familiar_phone" class="form-control-label">Teléfono de Familiar</label>
                <?php
                $attrphone = array('id' => 'familiar_phone', 'placeholder' => 'Teléfono de Familiar', 'class' => 'form-control input_phone', 'required'=>true, 'name'=>'familiar_phone');
                echo form_input($attrphone);
                 ?>
            </div>

          </div>
        </div>

        <div class="">
          <?= form_submit('submit', 'Siguiente', 'class="btn btn-info"') ?>
        </div>

        </div>
      </div>
    </div>
  </div>
</div>
<?= form_close(); ?>
