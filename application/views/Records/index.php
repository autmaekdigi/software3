<script type="text/javascript">

$(document).ready(function() {

    function fld(data)
    {
        var date = new Date(data);

        return date.toDateString();
    }
function longnames(name)
{
    return '<small>'+name+'</small>';
}


function actions(id)
{
  html = '<div class="text-center row-menu"><div class="btn-group text-left">';
  html += '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
  html += 'Acciones<span class="caret"></span></button><ul class="dropdown-menu pull-right" role="menu">'
  html += '<li><a href="<?= site_url('records/edit') ?>/'+id+'"><i class="fa fa-pencil"></i>Editar</a></li>'
  html += '<li><a href="<?= site_url('records/delete') ?>/'+id+'"><i class="fa fa-ban"></i>Borrar</a></li></ul></div></div>';

  return html;
}

  $('#recordTable').DataTable({
    "dom": '<"text-center"<"btn-group"B>><"clear"><"row"<"col-md-6"l><"col-md-6 pr0"p>r>t<"row"<"col-md-6"i><"col-md-6"p>><"clear">',
            "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            "order": [[ 0, "desc" ]],
            "pageLength": 10,
            "processing": true, "ServerSide": true,
            'ajax' : { url: '<?=site_url('records/getrecords/');?>', type: 'POST', "data": function ( d ) {
                d.<?=$this->security->get_csrf_token_name();?> = "<?=$this->security->get_csrf_hash()?>";
            }},
            "buttons": [
            { extend: 'copyHtml5', exportOptions: { columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ] } },
            { extend: 'excelHtml5', 'footer': true, exportOptions: { columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ] } },
            { extend: 'csvHtml5', 'footer': true, exportOptions: { columns: [ 0, 1, 2, 3, 4, 5, 6,7, 8] } },
            { extend: 'pdfHtml5', orientation: 'landscape', pageSize: 'A4', 'footer': true,
            exportOptions: { columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ] } }
            ],
            "columns": [
            { "data": "idExpediente" },
            { "data": "persona_cedula" },
            { "data": "nombre" },
            { "data": "CentroProcedencia_Desc", 'render':longnames},
            { "data": "AutoridadSentenciadora_Desc", 'render':longnames},
            { "data": "FechaIngreso" , 'render': fld},
            { "data": "MontoSentencia_Years" },
            { "data": "MontoSentencia_Month" },
            { "data": "action_id", "searchable": false, "orderable": false, 'render': actions}
            //{ "data": "Actions", "searchable": false, "orderable": false }
            ],
            "rowCallback": function( row, data, index ) {
                $(row).attr('idExpediente', data.sid);
                $(row).addClass('invoice_link');
            }

  });
} );
</script>

<div class="main-content">
  <div class="section__content section__content--p30">
    <div class="container-fluid">
    <h1><?= $module_name ?></h1>

    <table id="recordTable" >
      <thead>
        <tr>
          <th>ID</th>
          <th>Identificación</th>
          <th>Nombre</th>
          <th>Centro Penitenciario</th>
          <th>Número de expediente</th>
          <th>Fecha Ingreso</th>
          <th>Años Sentencia</th>
          <th>Meses Sentencia</th>
          <th>Acciones</th>
        </tr>
      </thead>

      <tbody>
      </tbody>

    </table>




  </div>
  </div>
</div>
