<link rel="stylesheet" href="<?= $assets ?>css/addRecord.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<script type="text/javascript" src="<?= $assets ?>vendor/jquery/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="<?=$assets ?>js/addRecord.js"></script>
<script type="text/javascript">
$( function() {
  $( ".datetime" ).datepicker({changeMonth: true,
changeYear:true});
} );
</script>

    <div class="wizard card-like">
        <?= form_open('records/edit') ?>
          <div class="wizard-header">
            <div class="row">
              <div class="col-xs-12">
                <h1 class="text-center">
                  <?= $module_name ?>
                </h1>
                <hr />
                <div class="steps text-center">
                  <div class="wizard-step active"></div>
                  <div class="wizard-step"></div>
                  <div class="wizard-step"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="wizard-body">
            <div class="step initial active">
              <div class="row">
                <div class="col-lg-6">
                  <div class="card">
                      <div class="card-header">
                          <strong>Información personal</strong>
                      </div>
                      <div class="card-body card-block">

                          <div class="form-group">
                              <label for="identification" class=" form-control-label">Identificaión</label>


                                    <?php

                                    $attrid = array('id' => 'identification', 'value' => (isset($_POST['identification']) ? $_POST['identification'] : $record_data->cedula), 'placeholder' => 'Identificación', 'class' => 'form-control', 'required'=>true, 'name'=> 'identification');
                                    echo form_input($attrid);
                                     ?>



                          </div>
                          <div class="form-group">
                              <label for="name" class=" form-control-label">Nombre</label>
                              <?php
                              $attrname = array('id' => 'name', 'value' => (isset($_POST['name']) ? $_POST['name'] : $record_data->nombre), 'placeholder' => 'Nombre', 'class' => 'form-control', 'required'=>true, 'name'=> 'name');
                              echo form_input($attrname);
                               ?>
                          </div>
                          <div class="form-group">
                              <label for="birthdate" class=" form-control-label">Fecha de nacimiento</label>
                              <?php
                              $attrbirth = array('id' => 'birthdate','value' => (isset($_POST['birthdate']) ? $_POST['birthdate'] : $record_data->FechaNac), 'placeholder' => 'Fecha de nacimiento', 'class' => 'form-control datetime', 'required'=>true, 'name'=> 'birthdate');
                              echo form_input($attrbirth);
                               ?>
                          </div>
                          <div class="row form-group">
                              <div class="col-8">
                                  <div class="form-group">
                                      <label for="status" class=" form-control-label">Estado Civil</label>
                                      <?php

                                      foreach ($status as $attr)
                                      {
                                        $attrstatus[$attr->EstadoCivil_Cod] = $attr->EstadoCivil_Desc;
                                      }

                                      echo form_dropdown('status',$attrstatus, (isset($_POST['status']) ? $_POST['status'] : $record_data->EstadoCivil_Ref), 'class="form-control select2"');
                                       ?>
                                  </div>
                              </div>
                              <div class="col-8">
                                  <div class="form-group">
                                      <label for="nationality" class=" form-control-label">Nacionalidad</label>
                                      <?php

                                      foreach ($nationality as $attr)
                                      {
                                        $attrnat[$attr->Nacionalidad_Cod] = $attr->Nacionalidad_Desc;
                                      }

                                      echo form_dropdown('nationality',$attrnat, (isset($_POST['nationality']) ? $_POST['nationality'] : $record_data->Nacionalidad_Ref), 'class="form-control select2"');
                                       ?>
                                    </div>
                              </div>
                          </div>
                          <div class="form-group">
                              <label for="scholarity" class=" form-control-label">Escolaridad</label>
                              <?php
                              foreach ($scholarity as $attr)
                              {
                                $attrsch[$attr->Escolaridad_Cod] = $attr->Escolaridad_Desc;
                              }

                              echo form_dropdown('scholarity',$attrsch, (isset($_POST['scholarity']) ? $_POST['scholarity'] : $record_data->Escolaridad_Ref), 'class="form-control select2"');
                               ?>

                             </div>
                      </div>
                  </div>
                </div>

                <div class="col-lg-6">
                  <div class="card">
                    <div class="card-header">
                        <strong>Localización</strong>
                    </div>
                    <div class="col-8">
                    <div class="form-group">
                        <label for="prov" class=" form-control-label">Provincia</label>
                        <?php

                        foreach ($provincia as $row)
                        {
                          $attrprov[$row->Provincia_Cod] = $row->Provincia_Desc;
                        }
                          echo form_dropdown('prov',$attrprov, (isset($_POST['prov']) ? $_POST['prov'] : '1'), 'class="form-control select2" id="provincia"');
                         ?>

                       </div>

                       <div class="form-group">
                           <label for="cant" class=" form-control-label">Cantón</label>
                           <?php

                           echo form_dropdown('cant','', (isset($_POST['cant']) ? $_POST['cant'] : '1'), 'class="form-control select2" id="canton"');
                            ?>
                        </div>
                        <div class="form-group">
                            <label for="distr" class=" form-control-label">Distrito</label>
                            <?php
                            echo form_dropdown('distr','', (isset($_POST['distr']) ? $_POST['distr'] : '1'), 'class="form-control select2" id="distrito"');
                             ?>
                        </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header">
                        <strong>Contacto</strong>
                    </div>
                    <div class="col-8">
                      <div class="form-group">
                          <label for="contact_phone" class="form-control-label">Teléfono de Localización</label>
                          <?php
                          $attrphone = array('id' => 'contact_phone','value' => (isset($_POST['contact_phone']) ? $_POST['contact_phone'] : ''), 'placeholder' => 'Teléfono de Localización', 'class' => 'form-control input_phone', 'required'=>true, 'name'=>'contact_phone');
                          echo form_input($attrphone);
                           ?>
                      </div>
                      <div class="form-group">
                          <label for="familiar_phone" class="form-control-label">Teléfono de Familiar</label>
                          <?php
                          $attrphone = array('id' => 'familiar_phone', 'value' => (isset($_POST['familiar_phone']) ? $_POST['familiar_phone'] : ''), 'placeholder' => 'Teléfono de Familiar', 'class' => 'form-control input_phone', 'required'=>true, 'name'=>'familiar_phone');
                          echo form_input($attrphone);
                           ?>
                      </div>

                    </div>
                  </div>
                  </div>
                </div>




            </div>
            <div class="step"><div class="row">
              <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <strong>Información Laboral</strong>
                    </div>
                    <div class="card-body card-block">
                        <div class="form-group">
                            <label for="company" class=" form-control-label">Empresa</label>
                            <?php
                            $attrcompany = array('id' => 'company', 'value' => (isset($_POST['company']) ? $_POST['company'] : $record_data->cedula), 'placeholder' => 'Empresa', 'class' => 'form-control', 'required'=>true, 'name'=>'company');
                            echo form_input($attrcompany);
                             ?>
                        </div>
                        <div class="form-group">
                            <label for="position" class=" form-control-label">Actividad Laboral</label>
                            <?php

                            foreach ($position as $row)
                            {
                              $attrposition[$row->ActividadLaboral_Cod] = $row->ActividadLaboral_Desc;
                            }
                            //$attrposition = array('id' => 'position', 'value' => (isset($_POST['position']) ? $_POST['position'] : ''), 'placeholder' => 'Actividad Laboral', 'class' => 'form-control', 'required'=>true, 'name'=>'position');
                            //echo form_input($attrposition);
                            echo form_dropdown('position', $attrposition, (isset($_POST['position']) ? $_POST['position'] : ''), 'class="form-control select2"');
                             ?>
                        </div>
                        <div class="form-group">
                            <label for="manager_name" class=" form-control-label">Nombre del Patrono</label>
                            <?php
                            $attrman_name = array('id' => 'manager_name', 'value' => (isset($_POST['manager_name']) ? $_POST['manager_name'] : ''), 'placeholder' => 'Nombre del Patrono', 'class' => 'form-control', 'name'=>'manager_name');
                            echo form_input($attrman_name);
                             ?>
                        </div>
                        <div class="row form-group">
                            <div class="col-8">
                              <div class="form-group">
                                  <label for="manager_phone" class="form-control-label">Teléfono de Patrono</label>
                                  <?php
                                  $attrphone = array('id' => 'manager_phone', 'value' => (isset($_POST['manager_phone']) ? $_POST['manager_phone'] : ''), 'placeholder' => 'Teléfono de Patrono', 'class' => 'form-control input_phone', 'required'=>true, 'name'=>'contact_phone');
                                  echo form_input($attrphone);
                                   ?>
                              </div>
                            </div>
                            <div class="col-8">
                                <div class="form-group">
                                    <label for="direction" class=" form-control-label">
                                    <input type="checkbox" class="form-check-input" name="direction" id="direction">
                                    ¿Localización laboral?</label>
                                  </div>
                            </div>

                            <div class="col-8" style="display: none;" id="localizacion_laboral">
                              <div class="form-group">
                                <label for="prov_lab">Provincia</label>
                                <?php
                                foreach ($provincia as $row)
                                {
                                  $attrprov[$row->Provincia_Cod] = $row->Provincia_Desc;
                                }
                                  echo form_dropdown('prov_lab',$attrprov, (isset($_POST['prov_lab']) ? $_POST['prov_lab'] : '1'), 'class="form-control select2" id="provincia_lab"');
                                ?>
                              </div>

                              <div class="form-group">
                                <label for="can_lab">Cantón</label>
                                <?= form_dropdown('can_lab','', (isset($_POST['can_lab']) ? $_POST['can_lab'] : '1'), 'class="form-control select2" id="canton_laboral"'); ?>
                              </div>

                              <div class="form-group">
                                <label for="dist_lab">Distrito</label>
                                <?= form_dropdown('dist_lab','', (isset($_POST['dist_lab']) ? $_POST['dist_lab'] : '1'), 'class="form-control select2" id="distrito_laboral"'); ?>
                              </div>
                            </div>

                            <div class="col-8">
                                <div class="form-group">
                                    <label for="police" class=" form-control-label">
                                    <input type="checkbox" class="form-check-input" name="police" id="police" >
                                    ¿Póliza laboral?</label>
                                  </div>

                                  <div class="form-group">
                                      <label for="pens" class=" form-control-label">
                                      <input type="checkbox" class="form-check-input" name="pens" id="pens">
                                      ¿Pensionado?</label>
                                  </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>

              <div class="col-lg-6">
                <div class="card">
                  <div class="card-header">
                      <strong>Información del Delíto</strong>
                  </div>
                  <div class="col-8">
                  <div class="form-group">
                      <label for="crime_date" class=" form-control-label">Fecha de Ingreso</label>
                      <?php
                      $attrcrimedate = array('id' => 'crime_date', 'value' => (isset($_POST['crime_date']) ? $_POST['crime_date'] : ''), 'placeholder' => 'Fecha de Ingreso', 'class' => 'form-control datetime', 'required'=>true, 'name'=> 'crime_date');
                      echo form_input($attrcrimedate);
                       ?>

                     </div>

                     <div class="form-group">
                         <label for="origin_center" class=" form-control-label">Centro de Procedencia</label>
                         <?php
                         foreach ($origin_center as $row)
                         {
                           $attrcenter[$row->CentroProcedencia_Cod] = $row->CentroProcedencia_Desc;
                         }

                         echo form_dropdown('origin_center',$attrcenter, (isset($_POST['origin_center']) ? $_POST['origin_center'] : '1'), 'class="form-control select2"');
                          ?>
                      </div>

                      <div class="form-group">
                          <label for="sentence_autority" class=" form-control-label">Autoridad Sentenciadora</label>
                          <?php
                          foreach ($sentence_autority as $row)
                          {
                            $attrcenter[$row->AutoridadSentenciadora_Cod] = $row->AutoridadSentenciadora_Desc;
                          }

                          echo form_dropdown('sentence_autority',$attrcenter, (isset($_POST['sentence_autority']) ? $_POST['sentence_autority'] : '1'), 'class="form-control select2"');
                           ?>
                       </div>

                      <div class="form-group">
                          <label for="victim" class=" form-control-label">Ofendido(s)</label>
                          <?php

                          // this will be displayed by the DB
                          echo form_input('victim',(isset($_POST['victim']) ? $_POST['victim'] : ''), 'class="form-control"');
                           ?>
                      </div>

                      <div class="form-group">
                          <label for="crime[]" class=" form-control-label">Delito(s)</label>
                          <?php
                          //$attrcrime['']= '';
                          foreach ($crime as $row)
                          {
                            $attrcrime[$row->Delitos_Cod] = $row->Delitos_Desc;
                          }
                          // this will be displayed by the DB
                          echo form_dropdown('crime[]',$attrcrime, (isset($_POST['crime']) ? $_POST['crime'] : ''), 'class="form-control select2 multiple" multiple="multiple"');
                           ?>
                      </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header">
                      <strong>Monto de sentencia</strong>
                  </div>
                  <div class="col-8">
                    <div class="form-group">
                        <label for="year_sentence" class="form-control-label">Años</label>
                        <?php
                        $attroyear = array();
                        for ($i=0; $i < 100 ; $i++)
                        {
                          $attroyear[$i] = $i .' Años';
                        }
                        echo form_dropdown('year_sentence', $attroyear, (isset($_POST['year_sentence']) ? $_POST['year_sentence'] : '0'), 'class="form-control select2" ');
                         ?>
                    </div>
                    <div class="form-group">
                        <label for="month_sentence" class="form-control-label">Meses</label>
                        <?php
                        $attrmonth = array();
                        for ($i=0; $i < 13 ; $i++)
                        {
                          $attrmonth[$i] = $i .' Meses';
                        }
                        echo form_dropdown('month_sentence', $attrmonth, (isset($_POST['month_sentence']) ? $_POST['month_sentence'] : '0'), 'class="form-control select2" ');
                         ?>
                    </div>

                  </div>
                </div>
                </div>
              </div>
            </div>

            <div class="step">
              <div class="row">
                <div class="col-lg-6">
                  <div class="card">
                      <div class="card-header">
                          <strong>Información del Delíto</strong>
                      </div>
                      <div class="card-body card-block">
                          <div class="form-group">
                              <label for="recordCode" class=" form-control-label">Número de expediente</label>
                              <?php
                              $attrrecord = array('id' => 'recordCode', 'value' => (isset($_POST['recordCode']) ? $_POST['recordCode'] : ''), 'placeholder' => 'Número de expediente', 'class' => 'form-control', 'name'=> 'recordCode');
                              echo form_input($attrrecord);
                               ?>
                        </div>
                      </div>
                  </div>
                  <div class="card">
                    <div class="card-header">
                        <strong>Cumple con Prisión</strong>
                    </div>
                    <div class="col-8">
                    <div class="form-group">
                        <label for="prison_date" class=" form-control-label">Fecha Ingreso</label>
                        <?php
                          $attrprison_date = array('id' => 'prison_date', 'value' => (isset($_POST['prison_date']) ? $_POST['prison_date'] : ''), 'placeholder' => 'Fecha de Ingreso', 'class' => 'form-control datetime', 'name'=> 'prison_date');
                          echo form_input($attrprison_date);
                         ?>

                       </div>

                       <div class="form-group">
                           <label for="prison_discount" class=" form-control-label">Descuento</label>
                           <?php
                             $attrprison_discount = array('id' => 'prison_discount', 'value'=> (isset($_POST['prison_discount']) ? $_POST['prison_discount'] : ''), 'placeholder' => 'Decuento', 'class' => 'form-control datetime', 'name'=> 'prison_discount');
                             echo form_input($attrprison_discount);
                            ?>
                        </div>
                        <div class="form-group">
                            <label for="prison_half" class=" form-control-label">Media Pena</label>
                            <?php
                              $attrprison_half = array('id' => 'prison_half', 'value' => (isset($_POST['prison_half']) ? $_POST['prison_half'] : ''), 'placeholder' => 'Media Pena', 'class' => 'form-control datetime', 'name'=> 'prison_half');
                              echo form_input($attrprison_half);
                             ?>
                        </div>

                        <div class="form-group">
                            <label for="sign_form" class=" form-control-label">Modalidad de Firma</label>
                            <?php
                              foreach ($sign_form as $row)
                              {
                                $attrsign_form[$row->ModalidadFirma_Cod] = $row->ModalidadFirma_Desc;
                              }
                              echo form_dropdown('sign_form',$attrsign_form,(isset($_POST['sign_form']) ? $_POST['sign_form'] : '') ,'class="form-control select2"');
                             ?>
                        </div>
                    </div>
                  </div>
                </div>



                <div class="col-lg-6">
                  <div class="card">
                    <div class="card-header">
                        <strong>Información Adicional</strong>
                    </div>
                    <div class="col-8">
                      <div class="form-group">
                          <label for="sexual_violence" class=" form-control-label">
                            <input type="checkbox" class="form-check-input" name="sexual_violence" id="sexual_violence">
                            ¿Violencia Sexual?
                        </label>
                      </div>
                      <div class="form-group">
                          <label for="drugs_atention" class=" form-control-label">
                            <input type="checkbox" class="form-check-input" name="drugs_atention" id="drugs_atention">
                            ¿Atención Drogas?
                        </label>
                      </div>

                      <div class="form-group">
                          <label for="domestic_violence" class=" form-control-label">
                            <input type="checkbox" class="form-check-input" name="domestic_violence" id="domestic_violence">
                            ¿Violencia Doméstica?
                        </label>
                      </div>

                      <div class="form-group">
                          <label for="physical_violence" class=" form-control-label">
                            <input type="checkbox" class="form-check-input" name="physical_violence" id="physical_violence" unchecked="<?= (bool)$record_data->ViolenciaFisica ?>">
                            ¿Violencia Fisica?
                        </label>
                      </div>
                      <div class="form-group">
                          <label for="injured" class=" form-control-label">
                            <input type="checkbox" class="form-check-input" name="injured" id="injured" checked="<?= $record_data->Discapacidad ?>">
                            Discapacidad
                        </label>
                      </div>

                      <div class="form-group">
                          <label for="native" class=" form-control-label">
                            <input type="checkbox" class="form-check-input" name="native" id="native" checked="<?= $record_data->Indigena ?>">
                            Indigena
                        </label>
                      </div>

                      <div class="form-group">
                          <label for="senior" class=" form-control-label">
                            <input type="checkbox" class="form-check-input" name="senior" id="senior" checked="<?= $record_data->AdultoMayor ?>">
                            Adulto Mayor
                        </label>
                      </div>

                      <div class="form-group">
                          <label for="lgbtqi" class=" form-control-label">
                            <input type="checkbox" class="form-check-input" name="lgbtqi" id="lgbtqi" checked="<?= $record_data->LGBTQI ?>">
                            LGBTQI
                        </label>
                      </div>

                    </div>
                  </div>
                  </div>
                </div>
            </div>
          </div>
          <div class="wizard-footer">
            <div class="row">
              <div class="col-xs-6 pull-left block-center">
                <button id="wizard-prev" style="display:none" type="button" class="btn btn-irv btn-irv-default">
                  Anterior
                </button>
              </div>
              <div class="col-xs-6 pull-right text-center">
                <button id="wizard-next" type="button" class="btn btn-irv btn-info">
                  Siguiente
                </button>
              </div>
              <div class="col-xs-6 pull-right block-center">

                <input type="submit" name="submit" value="Guardar" id="wizard-subm" style="display:none" type="button" class="btn btn-irv">

              </div>
            </div>
          </div>
        <?= form_close() ?>
      </div>

      <script type="text/javascript">

      $(document).ready(function(){
        //funcion para cargar los cantones dependiendo de la provincia
        $('select[name=\'prov\']').on('change', function() {

          var provincia = $('select[name=\'prov\']').val();

          $.ajax({
        type:'get',
        url: "<?=site_url('records/getCantones/');?>",
       data: { 'provincia': provincia, },
  success: function(data) {

    data = JSON.parse(data)
      html =''
  if (data.status == 200)
  {
    cantonData = data.data;

      for (i = 0; i < cantonData.length; i++)
    {
      html += '<option value="'+cantonData[i].Canton_Cod+'" attr="'+cantonData[i].CodCanton+'">' + cantonData[i].CantonDesc + '</option>';
    }

  }

  $("#canton").html(html);
  $('#canton').trigger("chosen:updated");
  $('select[name=\'cant\']').trigger('change');
  },
  error: function(){
  console.log('error');}
    })
  })

  $('select[name=\'cant\']').on('change', function() {

    var provincia = $('select[name=\'prov\']').val();
    var canton = this.options[this.selectedIndex].getAttribute('attr');
    console.log(canton +' '+ provincia);
    $.ajax({
  type:'get',
  url: "<?=site_url('records/getDistritos/');?>",
  data: { 'provincia': provincia, 'canton': canton},
  success: function(data) {

  data = JSON.parse(data)
  html =''
  if (data.status == 200)
  {
  distritoData = data.data;

  for (i = 0; i < distritoData.length; i++)
  {
  html += '<option value="'+distritoData[i].Distrito_Cod+'" attr="'+distritoData[i].DistritoCod+'">' + distritoData[i].Distrito_Desc + '</option>';
  }

  }


  $("#distrito").html(html);
  $('#distrito').trigger("chosen:updated");


  //$('select[name=\'city\']').trigger('change');
  },
  error: function(){
  console.log('error');
  }
    })
  })

/************************cambio para ubicacion laboral***********************************/

$('select[name=\'prov_lab\']').on('change', function() {

  var provincia = $('select[name=\'prov_lab\']').val();

  $.ajax({
type:'get',
url: "<?=site_url('records/getCantones/');?>",
data: { 'provincia': provincia, },
success: function(data) {

data = JSON.parse(data)
html =''
if (data.status == 200)
{
cantonData = data.data;

for (i = 0; i < cantonData.length; i++)
{
html += '<option value="'+cantonData[i].Canton_Cod+'" attr="'+cantonData[i].CodCanton+'">' + cantonData[i].CantonDesc + '</option>';
}

}

$("#canton_laboral").html(html);
$('#canton_laboral').trigger("chosen:updated");
$('select[name=\'can_lab\']').trigger('change');
},
error: function(){
console.log('error');}
})
})

$('select[name=\'can_lab\']').on('change', function() {

var provincia = $('select[name=\'prov_lab\']').val();
var canton = this.options[this.selectedIndex].getAttribute('attr');

$.ajax({
type:'get',
url: "<?=site_url('records/getDistritos/');?>",
data: { 'provincia': provincia, 'canton': canton},
success: function(data) {

data = JSON.parse(data)
html =''
if (data.status == 200)
{
distritoData = data.data;

for (i = 0; i < distritoData.length; i++)
{
html += '<option value="'+distritoData[i].Distrito_Cod+'" attr="'+distritoData[i].DistritoCod+'">' + distritoData[i].Distrito_Desc + '</option>';
}

}


$("#distrito_laboral").html(html);
$('#distrito_laboral').trigger("chosen:updated");


//$('select[name=\'city\']').trigger('change');
},
error: function(){
console.log('error');
}
})
})



  $('select[name=\'prov\']').trigger('change');
  $('select[name=\'cant\']').trigger('change');

  $('select[name=\'prov_lab\']').trigger('change');
  $('select[name=\'cant_lab\']').trigger('change');

  $('#direction').on('change', function(){

    if ($('#direction').is(':checked'))
    {
      $('#localizacion_laboral').slideDown();
    } else {
      $('#localizacion_laboral').slideUp();
    }
  })
})





      </script>
