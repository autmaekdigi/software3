
<script type="text/javascript">
$( function() {
  $( "#birthdate" ).datepicker();
} );
</script>
<div class="main-content">
  <div class="section__content section__content--p30">
    <div class="container-fluid">
    <h1><?= $module_name ?></h1>
    <?= form_open('records/add_secondstep') ?>
    <div class="row">
      <div class="col-lg-6">
        <div class="card">
            <div class="card-header">
                <strong>Información Laboral</strong>
            </div>
            <div class="card-body card-block">
                <div class="form-group">
                    <label for="company" class=" form-control-label">Empresa</label>
                    <?php
                    $attrcompany = array('id' => 'company', 'placeholder' => 'Empresa', 'class' => 'form-control', 'required'=>true, 'name'=>'company');
                    echo form_input($attrcompany);
                     ?>
                </div>
                <div class="form-group">
                    <label for="position" class=" form-control-label">Actividad Laboral</label>
                    <?php
                    $attrposition = array('id' => 'position', 'placeholder' => 'Actividad Laboral', 'class' => 'form-control', 'required'=>true, 'name'=>'position');
                    echo form_input($attrposition);
                     ?>
                </div>
                <div class="form-group">
                    <label for="manager_name" class=" form-control-label">Nombre del Patrono</label>
                    <?php
                    $attrman_name = array('id' => 'manager_name', 'placeholder' => 'Nombre del Patrono', 'class' => 'form-control', 'name'=>'manager_name');
                    echo form_input($attrman_name);
                     ?>
                </div>
                <div class="row form-group">
                    <div class="col-8">
                      <div class="form-group">
                          <label for="contact_phone" class="form-control-label">Teléfono de Patrono</label>
                          <?php
                          $attrphone = array('id' => 'contact_phone', 'placeholder' => 'Teléfono de Patrono', 'class' => 'form-control input_phone', 'required'=>true, 'name'=>'contact_phone');
                          echo form_input($attrphone);
                           ?>
                      </div>
                    </div>
                    <div class="col-8">
                        <div class="form-group">
                            <label for="direction" class=" form-control-label">¿Localización laboral?</label>
                            <input type="checkbox" class="form-control" name="direction" id="direction" value="direction">
                          </div>
                    </div>

                    <div class="col-8">
                        <div class="form-group">
                            <label for="police" class=" form-control-label">¿Póliza laboral?</label>
                            <input type="checkbox" class="form-control" name="police" id="police" value="police">
                          </div>

                          <div class="form-group">
                              <label for="pens" class=" form-control-label">¿Pensionado?</label>
                              <input type="checkbox" class="form-control" name="pens" id="pens" value="pens">
                          </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="scholarity" class=" form-control-label">Escolaridad</label>
                    <?php
                    $attrsch = array('1' => 'Primaria completa', '2' => 'Secundaria completa', '3' => 'Ninguna');// this will be displayed by the DB
                    echo form_dropdown('scholarity',$attrsch, '1', 'class="form-control select2"');
                     ?>

                   </div>
            </div>
        </div>
      </div>

      <div class="col-lg-6">
        <div class="card">
          <div class="card-header">
              <strong>Localización</strong>
          </div>
          <div class="col-8">
          <div class="form-group">
              <label for="prov" class=" form-control-label">Provincia</label>
              <?php
              $attrprov = array('1' => 'San Jose', '2' => 'Alajuela', '3' => 'Cartago', '4'=>'Heredia', '5' => 'Guanacaste', '6'=>'Puntarenas', '7'=> 'Limón');// this will be displayed by the DB
              echo form_dropdown('prov',$attrprov, '1', 'class="form-control select2"');
               ?>

             </div>

             <div class="form-group">
                 <label for="cant" class=" form-control-label">Cantón</label>
                 <?php
                 $attrcant = array('1' => 'San Jose');// this will be displayed by the DB
                 echo form_dropdown('cant',$attrcant, '1', 'class="form-control select2"');
                  ?>
              </div>
              <div class="form-group">
                  <label for="distr" class=" form-control-label">Distrito</label>
                  <?php
                  $attrcant = array('1' => 'San Jose');// this will be displayed by the DB
                  echo form_dropdown('distr',$attrcant, '1', 'class="form-control select2"');
                   ?>
              </div>
          </div>
        </div>
        <div class="card">
          <div class="card-header">
              <strong>Contacto</strong>
          </div>
          <div class="col-8">
            <div class="form-group">
                <label for="contact_phone" class="form-control-label">Teléfono de Localización</label>
                <?php
                $attrphone = array('id' => 'contact_phone', 'placeholder' => 'Teléfono de Localización', 'class' => 'form-control input_phone', 'required'=>true, 'name'=>'contact_phone');
                echo form_input($attrphone);
                 ?>
            </div>
            <div class="form-group">
                <label for="familiar_phone" class="form-control-label">Teléfono de Familiar</label>
                <?php
                $attrphone = array('id' => 'familiar_phone', 'placeholder' => 'Teléfono de Familiar', 'class' => 'form-control input_phone', 'required'=>true, 'name'=>'familiar_phone');
                echo form_input($attrphone);
                 ?>
            </div>

          </div>
        </div>

        <div class="">
          <?= form_submit('submit', 'Siguiente', 'class="btn btn-info"') ?>
        </div>

        </div>
      </div>
    </div>
  </div>
</div>
<?= form_close(); ?>
