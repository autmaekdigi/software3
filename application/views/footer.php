</div>
</div>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!--===============================================================================================-->
<!--===============================================================================================-->
<script src="<?= $assets ?>vendor/bootstrap/js/popper.js"></script>
<script src="<?= $assets ?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="<?= $assets ?>vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="<?= $assets ?>vendor/tilt/tilt.jquery.min.js"></script>
<script type="text/javascript" src="<?=$assets ?>js/datatables.min.js"></script>

<script >
  $('.js-tilt').tilt({
    scale: 1.1
  })

  $( ".select2" ).select2({
      theme: "bootstrap"
  });

  var element = document.getElementById('contact_phone');
  var maskOptions = {
    mask: '0000-0000'
  };
  var mask = IMask(element, maskOptions);

  var element = document.getElementById('familiar_phone');
  var maskOptions = {
    mask: '0000-0000'
  };
  var mask = IMask(element, maskOptions);

  var element = document.getElementById('manager_phone');
  var maskOptions = {
    mask: '0000-0000'
  };
  var mask = IMask(element, maskOptions);

</script>
<!--===============================================================================================-->
<script src="<?= $assets ?>vendor/animsition/animsition.min.js"></script>

<script src="<?= $assets ?>js/main.js"></script>
</body>
</html>
