<body>

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="<?= $assets ?>images/logo.png" alt="IMG">
				</div>

				<?php
				$attr = array('class' => 'login100-form validate-form');
				 echo form_open('auth/login', $attr); ?>

					<span class="login100-form-title">
						Iniciar Sesión
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<?php
						$attremail = array('type'=> 'text', 'name'=> 'identity', 'placeholder'=> 'Correo Electrónico', 'class' =>'input100');
						echo form_input($attremail);
						?>

						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<?php
						$attrpass= array('type'=> 'password', 'name'=> 'password', 'placeholder'=> 'Contraseña', 'class' =>'input100');
						echo form_input($attrpass);
						?>

						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>

					<div class="container-login100-form-btn">
						<?= form_submit('submit', 'Iniciar Sesión', array('class'=> 'login100-form-btn')) ?>

					</div>

					<div class="text-center p-t-12">
						<span class="txt1">
							¿Olvidó su
						</span>
						<a class="txt2" href="#">
							Contraseña?
						</a>
					</div>

				<?= form_close(); ?>
			</div>
		</div>
	</div>
