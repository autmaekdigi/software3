<!DOCTYPE html>
<html lang="en">
<head>
	<title><?= $module_name ?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="<?= $assets ?>images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= $assets ?>vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= $assets ?>fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= $assets ?>vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= $assets ?>vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= $assets ?>vendor/select2/select2.min.css">
<link rel="stylesheet" href="<?= $assets?>css/select2-bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= $assets ?>css/util.css">
	<link rel="stylesheet" type="text/css" href="<?= $assets ?>css/main.css">
<!--===============================================================================================-->
<link href="<?= $assets ?>css/animsition.min.css" rel="stylesheet" media="all">
<link href="<?= $assets ?>css/theme.css" rel="stylesheet" media="all">
<link href="<?= $assets ?>css/animate.css" rel="stylesheet" media="all">
<link href="<?= $assets ?>css/hamburgers.min.css" rel="stylesheet" media="all">
<link href="<?= $assets ?>css/slick.css" rel="stylesheet" media="all">
<link href="<?= $assets ?>css/perfect-scrollbar.css" rel="stylesheet" media="all">

<link rel="stylesheet" type="text/css" href="<?= $assets ?>js/datatables.min.css"/>
<link rel="stylesheet" href="<?= $assets ?>css/jqueryui.css">
<script src="<?= $assets ?>js/Imask.js"></script>
</head>


<?php



 if ($this->ion_auth->logged_in()): ?>

<body class="animsition">
	<div class="page-wrapper">


			<aside class="menu-sidebar d-none d-lg-block">
					<div class="logo">
							<a href="#">

									<img src="<?= $assets ?>images/logo.png" width="60" height="60" />
							</a>
					</div>
					<div class="menu-sidebar__content js-scrollbar1">
							<nav class="navbar-sidebar">
									<ul class="list-unstyled navbar__list">
											<li class="active has-sub">
													<a href="<?= site_url('home') ?>">
															<i class="fa fa-tachometer"></i>Inicio</a>
											</li>

											<li>
													<a href="<?= site_url('records') ?>">
															<i class="fa fa-file"></i>Mis Registro</a>
											</li>

											<li>
													<a href="<?= site_url('records/add') ?>">
															<i class="fa fa-check-square"></i>Agregar Registro</a>
											</li>
											<li>
													<a href="<?= site_url('reports') ?>">
															<i class="fa fa-line-chart"></i>Reportes</a>
											</li>

											<li>
													<a href="<?= site_url('settings') ?>">
															<i class="fa fa-gear"></i>Ajustes</a>
											</li>

											<li>
													<a href="<?= site_url('users') ?>">
															<i class="fa fa-user"></i>Usuarios</a>
											</li>

											<li>
													<a id="logout" href="<?= site_url('auth/logout'); ?>">
														<i class="fa fa-sign-out"></i>Cerrar Sesión
													</a>
											</li>

									</ul>
							</nav>
					</div>
			</aside>



		<script src="<?= $assets ?>vendor/jquery/jquery-3.2.1.min.js"></script>

	<div class="page-container">

		<?php endif; ?>
		<?php if($error) { echo "<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $error . "</div>"; } ?>
				<?php if($message) { echo "<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $message . "</div>"; } ?>
