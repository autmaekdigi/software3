<?php
$v = 'v=1';
if ($this->input->get('date_from'))
{
  $v .= '&date_from='.$this->input->get('date_from');
}

if ($this->input->get('date_to'))
{
  $v .= '&date_to='.$this->input->get('date_to');
}

if ($this->input->get('origin_center'))
{
  $v .= '&origin_center='.$this->inout->get('origin_center');
}

 ?>
<script type="text/javascript">

$( function() {
  $( ".datetime" ).datepicker({changeMonth: true,
changeYear:true});
} );

$(document).ready(function() {

    function fld(data)
    {
        var date = new Date(data);

        return date.toDateString();
    }
function longnames(name)
{
    return '<small>'+name+'</small>';
}


  $('#recordTable').DataTable({
    "dom": '<"text-center"<"btn-group"B>><"clear"><"row"<"col-md-6"l><"col-md-6 pr0"p>r>t<"row"<"col-md-6"i><"col-md-6"p>><"clear">',
            "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            "order": [[ 0, "desc" ]],
            "pageLength": 10,
            "processing": true, "ServerSide": true,
            'ajax' : { url: '<?=site_url('records/getrecords/');?>', type: 'POST', "data": function ( d ) {
                d.<?=$this->security->get_csrf_token_name();?> = "<?=$this->security->get_csrf_hash()?>";
            }},
            "buttons": [
            { extend: 'copyHtml5', exportOptions: { columns: [ 0, 1, 2, 3, 4, 5, 6 ] } },
            { extend: 'excelHtml5', 'footer': true, exportOptions: { columns: [ 0, 1, 2, 3, 4, 5, 6 ] } },
            { extend: 'csvHtml5', 'footer': true, exportOptions: { columns: [ 0, 1, 2, 3, 4, 5, 6] } },
            { extend: 'pdfHtml5', orientation: 'landscape', pageSize: 'A4', 'footer': true,
            exportOptions: { columns: [ 0, 1, 2, 3, 4, 5, 6 ] } }
            ],
            "columns": [
            { "data": "idExpediente" },
            { "data": "persona_cedula" },
            { "data": "nombre" },
            { "data": "CentroProcedencia_Desc", 'render':longnames},
            { "data": "AutoridadSentenciadora_Desc", 'render':longnames},
            { "data": "FechaIngreso" , 'render': fld},
            { "data": "MontoSentencia_Years" },
            { "data": "MontoSentencia_Month" },
            //{ "data": "Actions", "searchable": false, "orderable": false }
            ],
            "rowCallback": function( row, data, index ) {
                $(row).attr('idExpediente', data.sid);
                $(row).addClass('invoice_link');
            }

  });
} );
</script>

<div class="main-content">
  <div class="section__content section__content--p30">
    <div class="container-fluid">
    <h1><?= $module_name ?></h1>


    <?= form_open_multipart('reports/filter') ?>
      <div class="filter">
        <div class="row">
          <div class="col-6">
            <div class="form-group">
              <label for="date_from">Fecha Inicio</label>
              <?= form_input(array('placeholder' => 'Fecha Inicio', 'class'=> 'form-control datetime', 'id'=> 'date_from', 'name' =>'date_from')) ?>
            </div>
          </div>
          <div class="col-6">
            <div class="form-group">
              <label for="date_to">Fecha Final</label>
              <?= form_input(array('placeholder' => 'Fecha Final', 'class'=> 'form-control datetime', 'id'=> 'date_to', 'name' =>'date_to')) ?>
            </div>
          </div>

          <div class="col-6">
            <div class="form-group">
                <label for="origin_center" class=" form-control-label">Centro de Procedencia</label>
                <?php
                foreach ($origin_center as $row)
                {
                  $attrcenter[$row->CentroProcedencia_Cod] = $row->CentroProcedencia_Desc;
                }

                echo form_dropdown('origin_center',$attrcenter, (isset($_POST['origin_center']) ? $_POST['origin_center'] : '1'), 'class="form-control select2"');
                 ?>
             </div>
          </div>

          <div class="form-group">
            <?= form_submit('submit', 'Filtrar', 'class="btn btn-irv" id="wizard-subm"') ?>
          </div>

        </div>
      </div>
    <?= form_close() ?>

    <table id="recordTable" >
      <thead>
        <tr>
          <th>ID</th>
          <th>Identificación</th>
          <th>Nombre</th>
          <th>Centro Penitenciario</th>
          <th>Número de expediente</th>
          <th>Fecha Ingreso</th>
          <th>Años Sentencia</th>
          <th>Meses Sentencia</th>

        </tr>
      </thead>

      <tbody>
      </tbody>

    </table>




  </div>
  </div>
</div>
