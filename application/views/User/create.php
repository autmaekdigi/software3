<div class="main-content">
  <div class="section__content section__content--p30">
    <div class="container-fluid">
    <h1><?= $module_name ?></h1>


    <?= form_open_multipart('auth/create_user') ?>
      <div class="filter">
        <div class="row">
          <div class="col-6">
            <div class="form-group">
              <label for="first_name">Nombre</label>
              <?= form_input(array('placeholder' => 'Nombre', 'class'=> 'form-control', 'id'=> 'first_name', 'name' =>'first_name')) ?>
            </div>
          </div>
          <div class="col-6">
            <div class="form-group">
              <label for="last_name">Apellido</label>
              <?= form_input(array('placeholder' => 'Apellido', 'class'=> 'form-control', 'id'=> 'last_name', 'name' =>'last_name')) ?>
            </div>
          </div>

          <div class="col-6">
            <div class="form-group">
                <label for="email" class=" form-control-label">Correo Electronico</label>
                <?= form_input(array('placeholder' => 'Email', 'class'=> 'form-control', 'id'=> 'email', 'name' =>'email'));
                 ?>
             </div>
          </div>

          <div class="col-6">
            <div class="form-group">
                <label for="password" class=" form-control-label">Contraseña</label>
                <?= form_input(array('placeholder' => 'Contraseña', 'class'=> 'form-control', 'id'=> 'password', 'name' =>'password'));
                 ?>
             </div>
          </div>

          <div class="col-6">
            <div class="form-group">
                <label for="password_confirm" class=" form-control-label">Confirmar Contraseña</label>
                <?= form_input(array('placeholder' => 'Confirmar Contraseña', 'class'=> 'form-control', 'id'=> 'password_confirm', 'name' =>'password_confirm'));
                 ?>
             </div>
          </div>
          <div class="col-6">
            <div class="form-group">
                <label for="group_name" class=" form-control-label">Grupo de usuario</label>
                <?php
            $attrcenter = array('1'=>'Administrador', '2' => 'Administrador de registros');

                echo form_dropdown('group_name',$attrcenter, (isset($_POST['group_name']) ? $_POST['group_name'] : '1'), 'class="form-control select2"');
                 ?>
             </div>
          </div>


          <div class="form-group">
            <?= form_submit('submit', 'Guardar', 'class="btn btn-info"') ?>
          </div>

        </div>
      </div>
    <?= form_close() ?>

  </div>
  </div>
</div>
