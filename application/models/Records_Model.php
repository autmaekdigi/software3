<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 */
class Records_Model extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  public function addRecord($data=array())
  {

  }

  public function getStatus()
  {
    $q = $this->db->get('estadocivil');
    if ($q->num_rows())
    {
      foreach ($q->result() as $row)
      {
        $data[] = $row;
      }

      return $data;
    }

    return false;

  }


  public function getScholarity()
  {
    $q = $this->db->get('escolaridad');
    if ($q->num_rows())
    {
      foreach ($q->result() as $row)
      {
        $data[] = $row;
      }

      return $data;
    }

    return false;

  }

  public function getNationality()
  {
    $q = $this->db->get('nacionalidad');
    if ($q->num_rows())
    {
      foreach ($q->result() as $row)
      {
        $data[] = $row;
      }

      return $data;
    }

    return false;

  }

  public function getOriginCenter()
  {
    $q = $this->db->get('centroprocedencia');
    if ($q->num_rows())
    {
      foreach ($q->result() as $row)
      {
        $data[] = $row;
      }

      return $data;
    }

    return false;

  }

  public function getCrimes()
  {
    $q = $this->db->get('delitos');
    if ($q->num_rows())
    {
      foreach ($q->result() as $row)
      {
        $data[] = $row;
      }

      return $data;
    }

    return false;

  }

  public function getSignForm()
  {
    $q = $this->db->get('modalidadfirma');
    if ($q->num_rows())
    {
      foreach ($q->result() as $row)
      {
        $data[] = $row;
      }

      return $data;
    }

    return false;

  }

  public function getAutoritySentence()
  {
    $q = $this->db->get('autoridadsentenciadora');
    if ($q->num_rows())
    {
      foreach ($q->result() as $row)
      {
        $data[] = $row;
      }

      return $data;
    }

    return false;

  }

  public function getPositions()
  {
    $q = $this->db->get('actividadeslaborales');
    if ($q->num_rows())
    {
      foreach ($q->result() as $row)
      {
        $data[] = $row;
      }

      return $data;
    }

    return false;

  }


  public function getProvincia($id = null)
  {
    $q = $this->db->get('provincia');
    if ($q->num_rows())
    {
      foreach ($q->result() as $row)
      {
        $data[] = $row;
      }

      return $data;
    }
    return false;
  }

  public function getCantones($provincia = null)
  {
    $q = $this->db->get_where('canton', array('CodProvincia' => $provincia));
    if ($q->num_rows())
    {
      foreach ($q->result() as $row)
      {
        $data[] = $row;
      }

      return $data;
    }
    return false;
  }

  public function getDistritos($provincia = null, $canton = null)
  {
    $q = $this->db->get_where('distrito', array('CodProvincia' => $provincia, 'CodCanton' => $canton));
    if ($q->num_rows())
    {
      foreach ($q->result() as $row)
      {
        $data[] = $row;
      }

      return $data;
    }
    return false;
  }

/***************************Guardar Registros***********************/

public function SavePerson($data=array())
{
  if ($this->db->insert('persona',$data))
  {
    return true;
  }
  return false;
}
  public function SaveRecord($data=array())
  {
    if ($this->db->insert('delitosxpersona', $data))
    {
      return $this->db->insert_id();
    }
    return false;
  }


  public function SaveCrimes($data=array(), $id = null)
  {

    for ($i=0; $i < count($data) ; $i++)
    {
      $crime = array(
        'Delitos_Ref' => $data[$i],
        'IDregistro' =>$id
      );
      $this->db->insert('delitoscometidos', $crime);
    }
    return true;
  }

  public function SaveWork($data=array())
  {
    if ($this->db->insert('empleoxpersona', $data))
    {
      return true;
    }
    return false;
  }

  /***********************Buscar registros guardados***********************/

  public function getAllRecords($data = array())
  {
    //$q = $this->db->get_where('',$data);
    $this->db->select('idExpediente, persona_cedula, nombre, CentroProcedencia_Desc, AutoridadSentenciadora_Desc, FechaIngreso, MontoSentencia_Years, MontoSentencia_Month, idExpediente as action_id');
    $this->db->from('delitosxpersona');
    $this->db->join('autoridadsentenciadora', 'delitosxpersona.AutoridadSentenciadora_Ref = autoridadsentenciadora.AutoridadSentenciadora_Cod');
    $this->db->join('centroprocedencia', 'centroprocedencia.CentroProcedencia_Cod = delitosxpersona.CentroProcedencia_Ref');
    $this->db->join('persona', 'persona.cedula = delitosxpersona.persona_cedula');

    $q = $this->db->get();


    if ($q->num_rows())
    {
      foreach ($q->result() as $row)
      {
        $data[] = $row;
      }

      return $data;
    }
    return false;

  }

  /************************Funciones para edicion de registro*****************************/

  public function EditWork($data=array())
  {
    if ($this->db->update('empleoxpersona', $data))
    {
      return true;
    }
    return false;
  }

  public function EditPerson($data=array())
  {
    if ($this->db->update('persona', $data))
    {
      return true;
    }
    return false;
  }

  public function EditRecord($data=array())
  {
    if ($this->db->update('delitosxpersona', $data))
    {
      return true;
    }
    return false;
  }

  public function EditCrimes($data=array(), $id = null)
  {

    for ($i=0; $i < count($data) ; $i++)
    {
      $crime = array(
        'Delitos_Ref' => $data[$i],
        'IDregistro' =>$id
      );
      $this->db->insert('delitoscometidos', $crime);
    }
    return true;
  }

  public function getRecordById($id)
  {
    $this->db->select('*')
    ->from('delitosxpersona')
    ->join('persona', 'delitosxpersona.persona_cedula=persona.cedula')
    //->join('direccionxpersona', 'direccionxpersona.persona_cedula = persona.cedula')
    ->join('empleoxpersona', 'empleoxpersona.persona_cedula=persona.cedula')
    ->where('idExpediente', $id);
    $q = $this->db->get();

    if ($q->num_rows())
    {
      return $q->row();
    }
    return false;
  }


}
