<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->lang->load('auth');

	}

	public function index()
	{

			$this->data['module_name'] = 'Dashboard';
			$this->page_construct('home/index', $this->data);
	}
}
