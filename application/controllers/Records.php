<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Records extends MY_Controller {



	function __construct()
	{
			parent::__construct();
			$this->load->library('ion_auth');
			$this->load->library('form_validation');
			$this->load->model('records_model');

			if (!$this->ion_auth->logged_in()) {
				redirect('auth/index');
			}
	}

	public function index()
	{

			$this->data['module_name'] = 'Registros';
			$this->page_construct('records/index', $this->data);
	}


// esta funcion carga las vistas de agregrar un nuevo registro
  public function add()
  {
		//reglas para validacion de campos requeridos
		$this->form_validation->set_rules('identification',"Identificaión", 'trim|required|min_length[7]|max_length[25]');
		$this->form_validation->set_rules('name',"Nombre", 'trim|required|min_length[5]|max_length[150]');
		$this->form_validation->set_rules('birthdate',"Fehca de Nacimiento", 'trim|required');
		$this->form_validation->set_rules('status',"Estado Civil", 'trim|required');
		$this->form_validation->set_rules('nationality',"Nacionalidad", 'trim|required');
		$this->form_validation->set_rules('scholarity',"Escolaridad", 'trim|required');
		$this->form_validation->set_rules('contact_phone',"Teléfono de Contacto", 'trim|required|min_length[8]|max_length[25]');
		$this->form_validation->set_rules('familiar_phone',"Teléfono de Familiar", 'trim|required|min_length[8]|max_length[25]');
		$this->form_validation->set_rules('company',"Empresa", 'trim|required|min_length[3]|max_length[150]');
		$this->form_validation->set_rules('position',"Actividad Laboral", 'required');
		$this->form_validation->set_rules('manager_name',"Nombre del Patrono", 'trim|required|min_length[8]|max_length[150]');
		$this->form_validation->set_rules('crime_date',"Fecha de Ingreso", 'trim|required');
		$this->form_validation->set_rules('origin_center',"Centro de Procedencia", 'trim|required');
		$this->form_validation->set_rules('sentence_autority',"Autoridad Sentenciadora", 'trim|required');

		$this->form_validation->set_rules('victim',"Ofendido(s)", 'trim|required');
		$this->form_validation->set_rules('crime[]',"Delito(s)", 'required');
		$this->form_validation->set_rules('year_sentence',"Meses", 'trim|required');
		$this->form_validation->set_rules('month_sentence',"Años", 'trim|required');
		$this->form_validation->set_rules('prison_date',"Cumple con Prisión", 'trim|required');

		$this->form_validation->set_rules('sign_form',"Modalidad de Firma", 'trim|required');
		if ($this->form_validation->run() === TRUE) // si la validacion del formulario es verdadero se agregara el nuevo registro
		{
			// Esta data va a ser para el Registro criminal


			$person_data = array(
				'cedula' => $this->input->post('identification'),
				'nombre' => $this->input->post('name'),
				'FechaNac' => $this->input->post('birthdate'),
				'EstadoCivil_Ref' => $this->input->post('status'),
				'Nacionalidad_Ref' => $this->input->post('nationality'),
				'Escolaridad_Ref' => $this->input->post('scholarity'),
				'Discapacidad' => is_null($this->input->post('injured')) ? false : true,
				'Indigena' => is_null($this->input->post('native')) ? false : true,
				'AdultoMayor' => is_null($this->input->post('senior')) ? false : true,
				'LGBTQI' => is_null($this->input->post('lgbtqi')) ? false : true );
			$data = array(
				'Expediente_Cod' => $this->input->post('recordCode'),
		 		'persona_cedula' => $this->input->post('identification'),
			 	'CentroProcedencia_Ref' => $this->input->post('origin_center'),
				'AutoridadSentenciadora_Ref' => $this->input->post('sentence_autority'),
				'FechaIngreso' => date('Y-m-d',strtotime($this->input->post('prison_date'))),
				'MontoSentencia_Years' => $this->input->post('year_sentence'),
				'MontoSentencia_Month' => $this->input->post('month_sentence'),
				'CumplePrision' => date('Y-m-d',strtotime($this->input->post('prison_date'))),
				'CumpleDescuento' => date('Y-m-d', strtotime($this->input->post('prison_discount'))),
				'MediaPena' => date('Y-m-d', strtotime($this->input->post('prison_half'))),
				'ViolenciaSexual' => is_null($this->input->post('sexual_violence')) ? false : true,
				'AtencionDrogas' => is_null($this->input->post('drugs_atention')) ? false : true,
				'ViolenciaDomestica' => is_null($this->input->post('domestic_violence')) ? false :true,
				'ViolenciaFisica' => is_null($this->input->post('physical_violence')) ? false: true,
				'Usuarios_id' => $this->session->userdata('user_id'),
				'Ofendidos' => $this->input->post('victim'),
				'TIPOFIRMA' => $this->input->post('sign_form'),
			);


			$work = array(
				'persona_cedula' => $this->input->post('identification'),
				'Empresa' => $this->input->post('company'),
			 	'TipoSeguro_Ref' =>1,
				'ActividadLaboral_Ref' => $this->input->post('position'),
				'Nombre_Patron' => $this->input->post('manager_name'),
				'Poliza_Trabajo' => is_null($this->input->post('police')) ? false : true,
				'Pensionado' => is_null($this->input->post('pens')) ? false : true
			);


			$habitacion = array(
				'cedula_persona' => $this->input->post('identification'),
				'tipoDireccion' => 2,
				'Provincia_Ref' => $this->input->post('prov'),
				'Canton_Ref' => $this->input->post('cant'),
				'Distrito_Ref' => $this->input->post('dist'));


				$this->records_model->SavePerson($person_data);
				$record_id = $this->records_model->SaveRecord($data);
				$this->records_model->SaveCrimes($this->input->post('crimes[]'), $record_id);
				$this->records_model->SaveWork($work);

				$this->data['message'] = 'Registro agregado correctamente';

				redirect('records/add');

		}
		else
		{
			$this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->data['status'] = $this->records_model->getStatus();
			$this->data['scholarity'] = $this->records_model->getScholarity();
			$this->data['nationality'] = $this->records_model->getNationality();
			$this->data['origin_center'] = $this->records_model->getOriginCenter();
			$this->data['sentence_autority'] = $this->records_model->getAutoritySentence();
			$this->data['position'] = $this->records_model->getPositions();
			$this->data['provincia'] = $this->records_model->getProvincia();

			$this->data['crime'] = $this->records_model->getCrimes();
			$this->data['sign_form'] = $this->records_model->getSignForm();
			$this->data['module_name'] = 'Agregar Registros';
	    $this->page_construct('records/newRecord', $this->data);
		}

  }

	public function add_firststep()
	{
		$this->form_validation->set_rules('identification', 'identification', 'required');
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('birthdate', 'birthdate', 'required');
		$this->form_validation->set_rules('status', 'status', 'required');
		$this->form_validation->set_rules('nationality', 'nationality', 'required');
		$this->form_validation->set_rules('scholarity', 'scholarity', 'required');

		if ($this->form_validation->run() ==TRUE) {

			//do something in db
			$data = array();
			$this->data['module_name'] = 'Agregar Registro';
			$this->page_construct('records/newRecord', $this->data);



		} else {
			$this->session->set_flashdata('error', 'Error en el formulario '.validation_errors());
			redirect('records/newRecord');
		}
	}

	public function add_secondstep()
	{
		$this->data['module_name'] = 'Agregar Registro';
		$this->page_construct('records/add_secondstep', $this->data);

	}

	public function newRecord($id=null)
	{
		$this->data['module_name'] = "Agregar Registro";
		$this->page_construct('records/newRecord', $this->data);
	}



	public function getCantones($provincia=null)
	{
		if ($this->input->get('provincia')) {	$provincia = 	$this->input->get('provincia'); }

		if (!is_null($provincia))
		{
			$cantones = $this->records_model->getCantones($provincia);

			echo json_encode(array('status' => 200, 'data' => $cantones));
			return;
		}
			echo json_encode(array('status' => 400, 'data' => ''));
	}

	public function getDistritos($provincia=null, $canton = null)
	{
		if ($this->input->get('provincia')) {	$provincia = 	$this->input->get('provincia'); }
		if ($this->input->get('canton')) {	$canton = 	$this->input->get('canton'); }

		if (!is_null($provincia) && !is_null($canton))
		{
			$distritos = $this->records_model->getDistritos($provincia, $canton);

			echo json_encode(array('status' => 200, 'data' => $distritos));
			return;
		}
			echo json_encode(array('status' => 400, 'data' => ''));

	}


	public function getrecords()
	{
		$q = $this->records_model->getAllRecords();

		if ($q)
		{
			echo json_encode(array('status' => 200, 'data' => $q));
			return;
		}
		echo json_encode(array('status' => 400, 'data' => array()));
	}

	//funcion para edicion de registro
	public function edit($id=null)
	{
		if ($this->input->get('id')) { $id = $this->input->get('id'); }
		$record = $this->records_model->getRecordById($id);

		$this->form_validation->set_rules('identification',"Identificaión", 'trim|required|min_length[7]|max_length[25]');
		$this->form_validation->set_rules('name',"Nombre", 'trim|required|min_length[5]|max_length[150]');
		$this->form_validation->set_rules('birthdate',"Fehca de Nacimiento", 'trim|required');
		$this->form_validation->set_rules('status',"Estado Civil", 'trim|required');
		$this->form_validation->set_rules('nationality',"Nacionalidad", 'trim|required');
		$this->form_validation->set_rules('scholarity',"Escolaridad", 'trim|required');
		$this->form_validation->set_rules('contact_phone',"Teléfono de Contacto", 'trim|required|min_length[8]|max_length[25]');
		$this->form_validation->set_rules('familiar_phone',"Teléfono de Familiar", 'trim|required|min_length[8]|max_length[25]');
		$this->form_validation->set_rules('company',"Empresa", 'trim|required|min_length[3]|max_length[150]');
		$this->form_validation->set_rules('position',"Actividad Laboral", 'required');
		$this->form_validation->set_rules('manager_name',"Nombre del Patrono", 'trim|required|min_length[8]|max_length[150]');
		$this->form_validation->set_rules('crime_date',"Fecha de Ingreso", 'trim|required');
		$this->form_validation->set_rules('origin_center',"Centro de Procedencia", 'trim|required');
		$this->form_validation->set_rules('sentence_autority',"Autoridad Sentenciadora", 'trim|required');

		$this->form_validation->set_rules('victim',"Ofendido(s)", 'trim|required');
		$this->form_validation->set_rules('crime[]',"Delito(s)", 'required');
		$this->form_validation->set_rules('year_sentence',"Meses", 'trim|required');
		$this->form_validation->set_rules('month_sentence',"Años", 'trim|required');
		$this->form_validation->set_rules('prison_date',"Cumple con Prisión", 'trim|required');

		$this->form_validation->set_rules('sign_form',"Modalidad de Firma", 'trim|required');
		if ($this->form_validation->run() === TRUE) // si la validacion del formulario es verdadero se agregara el nuevo registro
		{
			// Esta data va a ser para el Registro criminal


			$person_data = array(
				'cedula' => $this->input->post('identification'),
				'nombre' => $this->input->post('name'),
				'FechaNac' => $this->input->post('birthdate'),
				'EstadoCivil_Ref' => $this->input->post('status'),
				'Nacionalidad_Ref' => $this->input->post('nationality'),
				'Escolaridad_Ref' => $this->input->post('scholarity'),
				'Discapacidad' => is_null($this->input->post('injured')) ? false : true,
				'Indigena' => is_null($this->input->post('native')) ? false : true,
				'AdultoMayor' => is_null($this->input->post('senior')) ? false : true,
				'LGBTQI' => is_null($this->input->post('lgbtqi')) ? false : true );
			$data = array(
				'Expediente_Cod' => $this->input->post('recordCode'),
		 		'persona_cedula' => $this->input->post('identification'),
			 	'CentroProcedencia_Ref' => $this->input->post('origin_center'),
				'AutoridadSentenciadora_Ref' => $this->input->post('sentence_autority'),
				'FechaIngreso' => date('Y-m-d',strtotime($this->input->post('prison_date'))),
				'MontoSentencia_Years' => $this->input->post('year_sentence'),
				'MontoSentencia_Month' => $this->input->post('month_sentence'),
				'CumplePrision' => date('Y-m-d',strtotime($this->input->post('prison_date'))),
				'CumpleDescuento' => date('Y-m-d', strtotime($this->input->post('prison_discount'))),
				'MediaPena' => date('Y-m-d', strtotime($this->input->post('prison_half'))),
				'ViolenciaSexual' => is_null($this->input->post('sexual_violence')) ? false : true,
				'AtencionDrogas' => is_null($this->input->post('drugs_atention')) ? false : true,
				'ViolenciaDomestica' => is_null($this->input->post('domestic_violence')) ? false :true,
				'ViolenciaFisica' => is_null($this->input->post('physical_violence')) ? false: true,
				'Usuarios_id' => $this->session->userdata('user_id'),
				'Ofendidos' => $this->input->post('victim'),
				'TIPOFIRMA' => $this->input->post('sign_form'),
			);


			$work = array(
				'persona_cedula' => $this->input->post('identification'),
				'Empresa' => $this->input->post('company'),
			 	'TipoSeguro_Ref' =>1,
				'ActividadLaboral_Ref' => $this->input->post('position'),
				'Nombre_Patron' => $this->input->post('manager_name'),
				'Poliza_Trabajo' => is_null($this->input->post('police')) ? false : true,
				'Pensionado' => is_null($this->input->post('pens')) ? false : true
			);


			$habitacion = array(
				'cedula_persona' => $this->input->post('identification'),
				'tipoDireccion' => 2,
				'Provincia_Ref' => $this->input->post('prov'),
				'Canton_Ref' => $this->input->post('cant'),
				'Distrito_Ref' => $this->input->post('dist'));


				$this->records_model->EditPerson($person_data);
				$record_id = $this->records_model->EditRecord($data);
				$this->records_model->EditCrimes($this->input->post('crimes[]'), $id);
				$this->records_model->EditWork($work);

				$this->data['message'] = 'Registro editado correctamente';

				redirect('records/index');
			}

		$this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		$this->data['status'] = $this->records_model->getStatus();
		$this->data['scholarity'] = $this->records_model->getScholarity();
		$this->data['nationality'] = $this->records_model->getNationality();
		$this->data['origin_center'] = $this->records_model->getOriginCenter();
		$this->data['sentence_autority'] = $this->records_model->getAutoritySentence();
		$this->data['position'] = $this->records_model->getPositions();
		$this->data['provincia'] = $this->records_model->getProvincia();

		$this->data['crime'] = $this->records_model->getCrimes();
		$this->data['sign_form'] = $this->records_model->getSignForm();
		$this->data['module_name'] = 'Editar Registros';
		$this->data['record_data'] = $record;
		$this->page_construct('records/edit', $this->data);


	}


}
