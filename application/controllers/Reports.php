<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('records_model');
		$this->load->helper('form');
	}

	public function index()
	{
			$this->data['module_name'] = 'Reportes';
			$this->data['origin_center'] = $this->records_model->getOriginCenter();
			$this->page_construct('reports/index', $this->data);
	}

  public function filter($data=null)
  {
$this->data['module_name'] = 'Reportes';
		$this->data['date_from'] = $this->input->post('date_from');
		$this->data['date_to'] = $this->input->post('date_to');
		$this->data['origin_center_selected'] = $this->input->post('origin_center');
	$this->data['origin_center'] = $this->records_model->getOriginCenter();


		$this->page_construct('reports/index', $this->data);
  }




}
