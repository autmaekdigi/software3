<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->lang->load('auth');

    if (!$this->ion_auth->logged_in())
    {
    	redirect('auth/index');
    }
	}

	public function index()
	{
			$this->data['module_name'] = 'Usuarios';
			$this->page_construct('user/index', $this->data);
	}

  public function CreateUser()
  {
    // code...
  }



}
