<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('ion_auth');
    

        $this->data['assets'] = base_url() . 'assets/';

            $dateFormats = array(
                'js_sdate' => 'yyyy-mm-dd',
                'php_sdate' => 'Y-m-d',
                'mysq_sdate' => '%Y-%m-%d',
                'js_ldate' => 'yyyy-mm-dd hh:ii:ss',
                'php_ldate' => 'Y-m-d H:i:s',
                'mysql_ldate' => '%Y-%m-%d %T'
                );

        $this->dateFormats = $dateFormats;
        $this->data['dateFormats'] = $dateFormats;
        //$this->Admin = $this->sim->in_group('admin') ? TRUE : NULL;
        //$this->data['Admin'] = $this->Admin;

    }

    function page_construct($page, $data = array(), $meta = array()) {
        $meta['message'] = isset($data['message']) ? $data['message'] : $this->session->flashdata('message');
        $meta['error'] = isset($data['error']) ? $data['error'] : $this->session->flashdata('error');
        $meta['warning'] = isset($data['warning']) ? $data['warning'] : $this->session->flashdata('warning');
        //$meta['Settings'] = $data['Settings'];
        $meta['assets'] = $data['assets'];
        $meta['dateFormats'] = $this->dateFormats;
        $meta['module_name'] = $data['module_name'];
        //$meta['events'] = $this->site->getUpcomingEvents();
        $this->load->view('header', $meta);
        $this->load->view($page, $data);
        $this->load->view('footer');
    }

}
